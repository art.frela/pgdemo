# pgdemo

Demonstrate postgres operations in transaction.

## how to use

See example code [internal/model/repo/pg/pg.go](https://gitlab.com/art.frela/pgdemo/-/blob/main/internal/model/repo/pg/pg.go#L82-149)

See gitlab-ci pipeline

```bash
pgdemo_618740975-usercreator-1  | 2022/08/21 14:15:25 [INFO], migrate up success
pgdemo_618740975-usercreator-1  | 2022/08/21 14:15:25 [INFO], read 5 users from file /myapp/users.csv
pgdemo_618740975-usercreator-1  | 2022/08/21 14:15:25 [INFO], start store 5 users
pgdemo_618740975-usercreator-1  | 2022/08/21 14:15:26 [INFO], success create 5 users
```
