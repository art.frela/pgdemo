package main

import (
	"context"
	"errors"
	"flag"
	"fmt"
	"log"
	"os"
	"os/signal"
	"syscall"

	"gitlab.com/art.frela/pgdemo/internal/model/repo/pg"
	"gitlab.com/art.frela/pgdemo/internal/users"
	"gitlab.com/art.frela/pgdemo/internal/users/utils/csv"

	"github.com/golang-migrate/migrate/v4"
	_ "github.com/golang-migrate/migrate/v4/database/postgres"
	_ "github.com/golang-migrate/migrate/v4/source/file"
)

const (
	testdata = "./internal/users/utils/testdata/users.csv"
	dbDefURL = "postgres://postgres:postgres@localhost:5432/users?sslmode=disable"
	//
	dbscripts = "file://migrations"
)

var (
	csvFile     = flag.String("f", testdata, "comma separated list of users: login,email,password")
	dburl       = flag.String("db", dbDefURL, "database url")
	scripts     = flag.String("sql", dbscripts, "database migrate sql scripts")
	needMigrate = flag.Bool("migrate", false, "flag need/not migrate up")
	email       = flag.String("email", "v", "email pattern (prefix) for search")
	limit       = flag.Int("lim", 3, "limit for find users")
)

func init() {
	flag.Parse()
}

func main() {
	if *needMigrate {
		if err := migrateUP(*scripts, *dburl); err != nil {
			log.Fatalln("[ERROR], database migrate UP error", err)
		}
		log.Println("[INFO], migrate up success")
	}

	ctx, stop := signal.NotifyContext(context.Background(), os.Interrupt, os.Kill, syscall.SIGTERM)
	defer stop()

	// open file
	f, err := os.Open(*csvFile)
	if err != nil {
		//nolint:gocritic
		log.Fatalln("[ERROR], open file", err)
	}

	// parse input
	newusers, err := csv.Read(f)
	if err != nil {
		log.Fatalln("[ERROR], parse csv file", err)
	}
	log.Printf("[INFO], read %d users from file %s", len(newusers), *csvFile)
	// create pg
	storage, err := pg.NewPG(ctx, *dburl)
	if err != nil {
		log.Fatalln("[ERROR], connect to database", *dburl, err)
	}

	// create usercreator
	userCreator := users.NewServer(storage)

	log.Printf("[INFO], start store %d users", len(newusers))
	if err := userCreator.CreateUsers(ctx, newusers); err != nil {
		log.Fatalln("[ERROR], create users", err)
	}
	log.Printf("[INFO], success create %d users", len(newusers))
	log.Printf("[INFO], try find users with email like %s", *email)

	users := userCreator.FindUsers(ctx, *email, *limit)
	fmt.Println(users)
}

func migrateUP(scripts, connString string) error {
	m, err := migrate.New(scripts, connString)
	if err != nil {
		return fmt.Errorf("for source %s migrate error %s", scripts, err)
	}
	// Migrate all the way up ...
	if err := m.Up(); err != nil && !errors.Is(err, migrate.ErrNoChange) {
		return fmt.Errorf("migrate.UP failed, %s", err)
	}
	return nil
}
