package model

import "context"

type UserRepoInterface interface {
	Store(context.Context, User) (*User, error)
	FindByEmail(context.Context, string, int) (Users, error)
}
