package pg

// pg.go contains postgres implementation of the repo interface

import (
	"context"
	"errors"
	"fmt"
	"log"
	"net"
	"time"

	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/pgxpool"
	"gitlab.com/art.frela/pgdemo/internal/model"
)

const (
	pgMaxConns                 int32         = 8
	pgMinConns                 int32         = 4
	pgHealthCheckPeriod        time.Duration = 1 * time.Minute
	pgMaxConnLifetime          time.Duration = 24 * time.Hour
	pgMaxConnIdleTime          time.Duration = 30 * time.Minute
	pgConnConfigConnectTimeout time.Duration = 1 * time.Second
)

var ErrNotFound error = errors.New("user not found")

// PG ...
type PG struct {
	db *pgxpool.Pool
}

// NewPG builder fog postgres implementation of the repo interface
func NewPG(ctx context.Context, url string) (*PG, error) {
	cfg, err := pgxpool.ParseConfig(url)
	if err != nil {
		return nil, fmt.Errorf("parse url error, %w", err)
	}
	// Pool соединений обязательно ограничивать сверху,
	// так как иначе есть потенциальная опасность превысить лимит соединений с базой.
	cfg.MaxConns = pgMaxConns
	cfg.MinConns = pgMinConns

	// HealthCheckPeriod - частота проверки работоспособности
	// соединения с Postgres
	cfg.HealthCheckPeriod = pgHealthCheckPeriod

	// MaxConnLifetime - сколько времени будет жить соединение.
	// Так как большого смысла удалять живые соединения нет,
	// можно устанавливать большие значения
	cfg.MaxConnLifetime = pgMaxConnLifetime

	// MaxConnIdleTime - время жизни неиспользуемого соединения,
	// если запросов не поступало, то соединение закроется.
	cfg.MaxConnIdleTime = pgMaxConnIdleTime

	// ConnectTimeout устанавливает ограничение по времени
	// на весь процесс установки соединения и аутентификации.
	cfg.ConnConfig.ConnectTimeout = pgConnConfigConnectTimeout

	// Лимиты в net.Dialer позволяют достичь предсказуемого
	// поведения в случае обрыва сети.
	cfg.ConnConfig.DialFunc = (&net.Dialer{
		KeepAlive: cfg.HealthCheckPeriod,
		// Timeout на установку соединения гарантирует,
		// что не будет зависаний при попытке установить соединение.
		Timeout: cfg.ConnConfig.ConnectTimeout,
	}).DialContext

	pool, err := pgxpool.ConnectConfig(ctx, cfg)
	if err != nil {
		return nil, fmt.Errorf("connect error, %w", err)
	}
	return &PG{db: pool}, nil
}

type TransactionFunc func(context.Context, pgx.Tx) error

// inTx создает транзакцию и передает её для использования в функцию f
// если в функции f происходит ошибка, транзакция откатывается
func (pg *PG) inTx(ctx context.Context, f TransactionFunc) error {
	transaction, err := pg.db.Begin(ctx)
	if err != nil {
		return err
	}

	err = f(ctx, transaction)
	if err != nil {
		rbErr := transaction.Rollback(ctx)
		if rbErr != nil {
			log.Print(rbErr)
		}
		return err
	}

	err = transaction.Commit(ctx)
	if err != nil {
		rbErr := transaction.Rollback(ctx)
		if rbErr != nil {
			log.Print(rbErr)
		}
		return err
	}

	return nil
}

//

func (pg *PG) Store(ctx context.Context, u model.User) (*model.User, error) {
	if err := pg.inTx(ctx, func(ctx context.Context, tx pgx.Tx) error {
		const userSQL = `insert into public.users (login, email, password)
						values ($1,$2,$3)
						returning id, creator, created_at;`

		hash := u.HashPass(model.MakeSalt(model.SaltLen), u.Password)
		var uid, creator string
		var createdAt time.Time
		err := tx.QueryRow(ctx, userSQL, u.Login, u.Email, hash).Scan(&uid, &creator, &createdAt)
		if err != nil {
			return fmt.Errorf("insert user error, %w", err)
		}
		if uid == "" {
			return fmt.Errorf("insert user error, empty id")
		}
		u.ID = uid
		u.Creator = creator
		u.CreatedAt = createdAt

		// log event

		const logSQL = `insert into public.dummy_log (user_id, event)
						values ($1,$2)
						returning id;`
		var logid int64
		if err := tx.QueryRow(ctx, logSQL, u.ID, "create new user").Scan(&logid); err != nil {
			return fmt.Errorf("insert dummy_log error, %w", err)
		}
		if logid == 0 {
			return fmt.Errorf("insert dummy_log error, empty id")
		}
		return nil
	}); err != nil {
		return nil, fmt.Errorf("store error, %w", err)
	}

	return &u, nil
}

func (pg *PG) FindByEmail(ctx context.Context, emailPattern string, limit int) (model.Users, error) {
	const sql = `select 
					id, 
					login, 
					email, 
					creator, 
					created_at, 
					modifier, 
					modified_at 
				from public.users 
					where email like $1
					order by email asc
					limit $2;`
	email := emailPattern + "%" // для работы like

	rows, err := pg.db.Query(ctx, sql, email, limit)
	if err != nil {
		return nil, fmt.Errorf("failed to query data: %w", err)
	}
	// Вызов Close нужен, чтобы вернуть соединение в пул
	defer rows.Close()

	users := make(model.Users, 0, 1)
	// rows.Next() итерируется по всем строкам, полученным из базы.
	for rows.Next() {
		var u model.User

		// Scan записывает значения столбцов в свойства структуры model.User
		err = rows.Scan(&u.ID, &u.Login, &u.Email, &u.Creator, &u.CreatedAt, &u.Modifier, &u.ModifiedAt)
		if err != nil {
			return nil, fmt.Errorf("failed to scan row: %w", err)
		}

		users = append(users, u)
	}

	// Проверка, что во время выборки данных не происходило ошибок
	if rows.Err() != nil {
		return nil, fmt.Errorf("failed to read response: %w", rows.Err())
	}

	return users, nil
}
