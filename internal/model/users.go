package model

import (
	"bytes"
	"crypto/rand"
	"fmt"
	"strings"
	"time"

	"golang.org/x/crypto/argon2"
)

const (
	SaltLen int = 15
)

type User struct {
	ID       string `json:"id,omitempty"`
	Login    string `json:"login,omitempty"`
	Email    string `json:"email,omitempty"`
	Password string `json:"-"`
	Metadata
}

// HashPass calculate password hash with salt
func (u *User) HashPass(salt []byte, plainPassword string) []byte {
	hashedPass := argon2.IDKey([]byte(plainPassword), salt, 1, 64*1024, 4, 32)
	return append(salt, hashedPass...)
}

// CheckPass compare hash and string password
func (u *User) CheckPass(passHash []byte, plainPassword string) bool {
	salt := make([]byte, SaltLen)
	copy(salt, passHash)
	userHash := u.HashPass(salt, plainPassword)
	return bytes.Equal(userHash, passHash)
}

// helpers

// nolint:errcheck
func MakeSalt(n int) []byte {
	salt := make([]byte, n)
	rand.Read(salt)
	return salt
}

type Users []User

func (uu Users) String() string {
	res := make([]string, len(uu))
	for i, u := range uu {
		res[i] = fmt.Sprintf("user %s (id=%s; email=%s; created at %s)", u.Login, u.ID, u.Email, u.CreatedAt)
	}
	res = append(res, fmt.Sprintf("total: %d user(s)", len(uu)))
	return strings.Join(res, "\n")
}

type Metadata struct {
	Creator    string     `json:"creator,omitempty"`
	CreatedAt  time.Time  `json:"created_at,omitempty"`
	Modifier   *string    `json:"modifier,omitempty"`
	ModifiedAt *time.Time `json:"modified_at,omitempty"`
}
