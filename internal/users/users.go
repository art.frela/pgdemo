package users

import (
	"context"
	"fmt"
	"log"

	"gitlab.com/art.frela/pgdemo/internal/model"
)

type Server struct {
	storage model.UserRepoInterface
}

func NewServer(storage model.UserRepoInterface) *Server {
	return &Server{storage: storage}
}

func (s *Server) CreateUsers(ctx context.Context, uu model.Users) error {
	for _, u := range uu {
		if _, err := s.storage.Store(ctx, u); err != nil {
			return fmt.Errorf("store user %s error %w", u.Login, err)
		}
	}
	return nil
}

func (s *Server) FindUsers(ctx context.Context, email string, limit int) model.Users {
	users, err := s.storage.FindByEmail(ctx, email, limit)
	if err != nil {
		log.Println("[ERROR], storage FindByEmail", err)
		return nil
	}
	return users
}
