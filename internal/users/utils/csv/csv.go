package csv

import (
	"encoding/csv"
	"fmt"
	"io"

	"gitlab.com/art.frela/pgdemo/internal/model"
)

// login,email,password

const (
	rowLen = 3
)

func validate(src []string) error {
	if len(src) != rowLen {
		return fmt.Errorf("row invalid, len=%d, want %d", len(src), rowLen)
	}
	return nil
}

// Read reads csv comma separated list and parse to model.User.
func Read(r io.Reader) (model.Users, error) {
	cr := csv.NewReader(r)
	users := make(model.Users, 0)
	for {
		record, err := cr.Read()
		if err == io.EOF {
			break
		}
		if err != nil {
			return nil, fmt.Errorf("read csv row error, %w", err)
		}
		if err := validate(record); err != nil {
			return nil, fmt.Errorf("csv row error, %w", err)
		}

		users = append(users, model.User{Login: record[0], Email: record[1], Password: record[2]})
	}
	return users, nil
}
